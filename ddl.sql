CREATE TABLE events(   
  event_id INT ,   
  event_name varchar(75),   
  visitors varchar(25),   
  properties json,   
  created_at timestamp
);  

create table events_update(
  event_id int, 
  event_name varchar(500), 
  visitors int, 
  properties_page varchar(500), 
  properties_amount int, 
  created_at timestamp
  );


create table sample(
  name json,
  age int, 
  sex varchar(300),
  created_at timestamp
  );



create table sample_updated (
  firstname varchar(100), 
  lastname varchar(100), 
  age int, 
  sex varchar(100), 
  created_at timestamp
  );

